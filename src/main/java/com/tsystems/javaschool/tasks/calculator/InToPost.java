package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

class InToPost {
    private Stack<Character> theStack;
    private String input;
    private StringBuilder output = new StringBuilder();


    InToPost(String in) {
        input = in;
        theStack = new Stack<>();
    }

    String doTrans(){
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            if(ch == ' ')
                continue;
            if(ch == '+' || ch == '-'){
                output.append(" ");
                getOperation(ch, 1);
                continue;
            }
            if(ch == '*' || ch == '/'){
                output.append(" ");
                getOperation(ch, 2);
                continue;
            }
            if(ch == '('){
                theStack.push(ch);
                continue;
            }
            if(ch == ')'){
                int res = gotParenthesis();
                if(res == 0) return "";
            } else {
                output.append(ch);
            }

        }
        while (!theStack.isEmpty()){
            output.append(' ');
            output.append(theStack.pop());
        }
        return output.toString();
    }

    private void getOperation(char opThis, int prec1) {
        while (!theStack.isEmpty()) {
            char opTop = theStack.pop();
            if (opTop ==  '(') {
                theStack.push(opTop);
                break;
            }else{
                int prec2;
                if (opTop == '+' || opTop == '-')
                    prec2 = 1;
                else
                    prec2 = 2;
                if (prec2 < prec1){
                    theStack.push(opTop);
                    break;
                }else{
                    output.append(opTop);
                    output.append(" ");
                }
            }
        }
        theStack.push(opThis);
    }

    private int gotParenthesis() {
        if(theStack.size() < 2)
            return 0;
        while (!theStack.isEmpty()) {
            char chx = theStack.pop();
            if (chx == '(') {
                break;
            }else{
                output.append(" ");
                output.append(chx);
            }
        }
        return 1;
    }
}

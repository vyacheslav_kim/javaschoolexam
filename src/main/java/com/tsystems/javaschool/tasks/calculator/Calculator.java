package com.tsystems.javaschool.tasks.calculator;

import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private Stack<Double> stack = new Stack<>();


    public String evaluate(String statement) {
        if (statement == null){
            return null;
        }

        double val;
        double tmpResult = 0;
        double num1, num2;

        InToPost converter = new InToPost(statement);
        String postfix = converter.doTrans();

        if(postfix.isEmpty()) {
            return null;
        }
        String[] tmp = postfix.split(" ");
        for (String aTmp : tmp) {
            if (aTmp.isEmpty()) {
                return null;
            }
            if (!aTmp.equals("+") && !aTmp.equals("-") &&
                    !aTmp.equals("*") && !aTmp.equals("/")) {
                try {
                    val = Double.valueOf(aTmp);
                } catch (NumberFormatException ex) {
                    return null;
                }
                stack.push(val);
            } else {
                num2 = stack.pop();
                num1 = stack.pop();
                if (aTmp.equals("+")) {
                    tmpResult = num1 + num2;
                }
                if (aTmp.equals("-")) {
                    tmpResult = num1 - num2;
                }
                if (aTmp.equals("*")) {
                    tmpResult = num1 * num2;
                }
                if (aTmp.equals("/")) {
                    if (num2 == 0) {
                        return null;
                    }
                    tmpResult = num1 / num2;
                }
                stack.push(tmpResult);
            }
        }
        String check = stack.peek().toString();
        if (check.charAt(check.length() - 1) == '0'){
            return String.valueOf(stack.pop().intValue());
        }
        if (String.valueOf(stack.peek() / stack.peek().intValue()).length() > 5){
            return String.format(Locale.US, "%.4f", stack.pop());
        }
        return stack.pop().toString();
    }
}

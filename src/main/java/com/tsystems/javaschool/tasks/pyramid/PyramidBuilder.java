package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();
        double levels = (Math.sqrt(1 + 8 * size) - 1) / 2;
        if (levels != Math.ceil(levels)) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int count = 0;
        int[][] res = new int[(int)levels][(int)levels * 2 - 1];
        int start = (res[0].length) / 2;
        for (int i = 0; i < levels; i++) {
            int begin = start;
            for (int j = 0; j <= i; j ++) {
                res[i][begin] = inputNumbers.get(count);
                count++;
                begin += 2;
            }
            start--;
        }
        return res;
    }

}
